console.log("Loading background script");

/**
 * 
 * @param {*} sentence 
 * @param {*} reversoCallback({substitution, probability, start})
 */
function reverso(sentence, reversoCallback) {
    console.log("Requesting reverso");
    var xhr = new XMLHttpRequest();
    var url = "http://orthographe.reverso.net/RISpellerWS/RestSpeller.svc/v1/CheckSpellingAsXml/language=fra?outputFormat=json&doReplacements=true&interfLang=fra&dictionary=both&spellOrigin=interactive&includeSpellCheckUnits=true&includeExtraInfo=true&isStandaloneSpeller=true";
    var reverso_proba = 0;
    var reverso_substitution = "";
    var reverso_start = 0;
    xhr.open("POST", url, true);
    xhr.setRequestHeader("Created", "01/01/0001 00:00:00");
    xhr.setRequestHeader("Username", "OnlineSpellerWS");
    xhr.setRequestHeader("Content-Type", "charset=utf-8");
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {
            var json = JSON.parse(xhr.responseText);
            parser = new DOMParser();
            xmlDoc = parser.parseFromString(json.Corrections, "text/xml");
            //Vérifier que pas + d'une erreur et choisir celle qui a la plus grande probabilité.
            if (xmlDoc.getElementsByTagName("error").length > 0) {
                reverso_substitution = xmlDoc.getElementsByTagName("error")[0].getAttributeNode("substitution").nodeValue;
                reverso_proba = xmlDoc.getElementsByTagName("error")[0].getAttributeNode("proba").nodeValue;
                reverso_start = xmlDoc.getElementsByTagName("error")[0].getAttributeNode("start").nodeValue;
            }
            reversoCallback(reverso_substitution, reverso_proba, reverso_start, json);
        }
    };
    xhr.send(sentence);
}

/**
 * 
 * @param {*} sentence 
 * @param {*} cordialCallback([alternative, percent, start]);
 */
function cordial(sentence, cordialCallback) {
    console.log("Requesting cordial");

    var cordial_proba = 0;
    var cordial_substitution = "";
    var cordial_res = "";

    var url = "http://195.154.100.114:80/api/textchecker/correct_logged";
    var httpRequest = new XMLHttpRequest();
    // false désactive l'asynchronisme
    httpRequest.open("POST", url, false);
    httpRequest.setRequestHeader("Content-Type", "application/xml; charset=utf-8");
    httpRequest.send("<RequestData><details>" + sentence + "</details><userlogin>AmisAutheurMerci@aut.fr</userlogin></RequestData>");
    var xmlRequest = httpRequest.responseXML;
    if (xmlRequest) {
        var responseData = xmlRequest.childNodes.item(0);
        var textCorrectedNode = responseData.childNodes.item(0).childNodes.item(0);
        var xmlCorrected = new DOMParser().parseFromString(textCorrectedNode.textContent, 'text/xml');
        var errors = xmlCorrected.querySelectorAll("error");

        var returnErrors = [];
        var start = 0;
        for (var i = 0; i < errors.length; i++) {
            var error = errors[i];
            var message = error.querySelector("message").textContent;
            var alternative = error.getAttribute("substitution");
            var percent = parseInt(error.getAttribute("proba"));
            start = parseInt(error.getAttribute("start"));
            var end = parseInt(error.getAttribute("end"));
            if (!(error.getAttribute("code_domaine_erreur") === "20.99" || error.getAttribute("code_domaine_erreur") === "20.69" || error.getAttribute("code_domaine_erreur") === "80.22" || error.getAttribute("code_domaine_erreur") === "80.31" || error.getAttribute("code_domaine_erreur") === "20.47")) {
                returnErrors.push(alternative, percent, start);
            }
        }
        var cord_elem_temp = 0;;
        var place_element_cord = 0;
        cordialCallback(returnErrors);
    }
}

function getMessage(request, sender, sendResponse){
    console.log(request);
    var phrase = request.phrase;
    if (request.command == "reverso") {
        reverso(phrase, function(substitution, proba, start, json){
            console.log("Sending reverso response");
            sendResponse({reverso_substitution: substitution, reverso_proba: proba, reverso_start: start, json: json});
            console.log("Sent response");
        });
    } else if (request.command == "cordial") {
        cordial(phrase, function(returnErrors){
            console.log("Sending cordial response");
            sendResponse(returnErrors);
        });
    }
    return true;
}

chrome.runtime.onMessage.addListener(getMessage);