console.log("Loading new injection...");

class VoltaireSolver {

    constructor() {
        this.sentenceDetectionDelay = 100;
        this.clickFixedDelay = 10000;
        this.clickRandomDelayRange = 5000;
        this.storage = chrome.storage.local;

        this.reversoLoaded = false;
        this.cordialLoaded = false;

        this.place_element_cordial = null;
        this.place_element_reverso = null;
        this.mots = null;
        this.sentence = "";
        this.auto = false;
        this.motauto = null;
        this.reverso_proba = 0;
        this.cordial_proba = 0;
        this.reprogram = false;

        console.log("Loading Voltaire Solver !")
        this.start();
    }

    start() {
        this.removePopup();
        this.sentence = "";
        this.waiting = setInterval(this.waitForSentence.bind(this), this.sentenceDetectionDelay);
    }

    registerListeners() {
        console.log("Registering listeners");
        var words = document.getElementsByClassName("pointAndClickSpan");
        var self = this;
        var clearAndListen = function () {
            self.clearUI();
            if (this.auto) {
                self.removePopup();
            }
            setTimeout(function () {
                var button = document.getElementById("btn_question_suivante");
                if (button != null && button.style.display != "none") {
                    button.addEventListener('click', function () {
                        self.start();
                    });
                }
                else
                {
                    setTimeout(function () {
                    self.start();
                    }, 10);
                }
            }, 1000);
        }
        for (let i = 0; i < words.length; i++) {
            words[i].addEventListener('click', clearAndListen, false);
        }
        var btn_pas_de_faute = document.getElementById("btn_pas_de_faute");
        if (btn_pas_de_faute != null) {
            btn_pas_de_faute.addEventListener('click', clearAndListen);
        }
    }

    waitForSentence() {
        console.log("Waiting...");
        this.reversoLoaded = false;
        this.cordialLoaded = false;
        this.mots = document.getElementsByClassName("pointAndClickSpan");
        if (this.mots.length > 0) {
            this.reprogram = false;
            var sentence = "";
            var nb_elements = this.mots.length;
            for (let i = 0; i < nb_elements; i++) {
                sentence += this.mots[i].textContent;
            }
            if (sentence != this.sentence) {
                this.sentence = sentence;
                clearInterval(this.waiting);
                this.injectUI();
                var self = this;
                this.storage.get(this.sentence,function(result){
                Object.prototype.isEmpty = function() {
                    for(var key in this) {
                        if(this.hasOwnProperty(key))
                            return false;
                        }
                    return true;
                }
                console.log(result);
                console.log(result.isEmpty());
                if (!result.isEmpty())
                {
                    /* SENTENCE ALREADY STORED*/
                    console.log("Loaded.");
                    console.log("result[self.sentence] : " + result[self.sentence]);
                    clearInterval(self.loading);
                    document.getElementById("myreversoDiv").style.display = "none";
                    document.getElementById("mycordialDiv").style.display = "none";
                    document.getElementById("myvoltaireDiv").style.display = "";
                    console.log(sentence);
                    console.log("Hyptohetique place : " + result[self.sentence]);
                    console.log(result);
                    if (result[self.sentence] != -1)
                    {
                        self.mots[result[self.sentence]].style.color = "red";
                        document.getElementById("voltaire-reponse-info").innerHTML = "Erreur sur '" + self.mots[result[self.sentence]].innerHTML + "' avec une probabilité de 100%";
                    }else
                    {
                        document.getElementById("voltaire-reponse-info").innerHTML = "Pas d'erreur.";
                    }
                    self.finishLoading();                 
                    self.registerListeners();
                    self.motauto = result[self.sentence];
                    if (self.auto){
                        setTimeout(self.validateAnswer.bind(self), self.Randomize(self.clickFixedDelay,self.clickRandomDelayRange), result[self.sentence]);
                    }
                }
                else
                {
                    //Appel à reverso et cordial
                    document.getElementById("myreversoDiv").style.display = "";
                    document.getElementById("mycordialDiv").style.display = "";
                    document.getElementById("myvoltaireDiv").style.display = "none";
                    self.requestData(sentence);
                    self.loading = setInterval(self.detectLoaded.bind(self), 100);
                    self.registerListeners();
                }

                });
            }
        }
        else if (document.getElementById("btn_apprentissage_autres_niveaux") != null && !this.reprogram)
        {
            this.reprogram = true;
            setTimeout(function () {
                document.getElementById("btn_apprentissage_sortir").click();
                console.log("Sortir");
            }, 5000);
            setTimeout(function () {
                if (document.getElementsByClassName("notStarted nextIsStandard")[0] == document.getElementById("activityCellDiv_13")) {
                    document.getElementById("productTab_4").click();
                    document.getElementsByClassName("activity-selector-reinit-button")[3].click();
                    document.getElementsByClassName("moduleListBox")[0].selectedIndex = 0;
                    document.getElementsByClassName("dialogOkButton")[0].click();
                    document.getElementById("btn_oui").click();
                    setTimeout(function () {
                        document.getElementById("productTab_3").click();
                        document.getElementsByClassName("activity-selector-reinit-button")[2].click();
                        document.getElementsByClassName("moduleListBox")[0].selectedIndex = 0;
                        document.getElementsByClassName("dialogOkButton")[0].click();
                        document.getElementById("btn_oui").click();
                        setTimeout(function () {
                            document.getElementById("productTab_2").click();
                            document.getElementsByClassName("activity-selector-reinit-button")[1].click();
                            document.getElementsByClassName("moduleListBox")[0].selectedIndex = 0;
                            document.getElementsByClassName("dialogOkButton")[0].click();
                            document.getElementById("btn_oui").click();
                            setTimeout(function () {
                                document.getElementById("productTab_1").click();
                                document.getElementsByClassName("activity-selector-reinit-button")[0].click();
                                document.getElementsByClassName("moduleListBox")[0].selectedIndex = 0;
                                document.getElementsByClassName("dialogOkButton")[0].click();
                                document.getElementById("btn_oui").click();
                                setTimeout(function () {
                                    document.getElementsByClassName("singleRunnable")[0].click();
                                }, 20000);
                            }, 20000);
                        }, 20000);
                    }, 20000);
                }
                else {
                    document.getElementsByClassName("singleRunnable")[0].click();
                }
                console.log("Je rerentre");
            }, 20000);
        }
    }

    clearUI() {
        if (document.getElementById("voltaire-solver") != null) {
            document.getElementById("voltaire-solver").remove();
        }
    }

    Randomize(number, maxdiff) {
        if (Math.random() > 0.5){
            number += maxdiff*Math.random();
        }else{
            number -= maxdiff*Math.random();
        } 
        return number; 
    }

    removePopup() {
        let background = document.getElementsByClassName("dialogBackground");
        if (background.length != 0) {    
            background[0].remove();
        }
        let popup = document.getElementsByClassName("popupPanel");
        let nb_popup = popup.length;
        for (let i = 0; i < nb_popup; i++) {
            popup[i].remove();
        }
    }

    injectUI() {
        // Main div
        var sheetContainer = document.getElementsByClassName("sheetContainer")[0];
        var uiContainer = document.createElement("div");
        sheetContainer.appendChild(uiContainer);
        uiContainer.id = "voltaire-solver";
        uiContainer.style.marginTop = "40px";
        uiContainer.style.padding = "20px";
        uiContainer.style.textAlign = "center";

        var title = document.createElement("h2");
        title.innerText = "Voltaire Solver";
        uiContainer.appendChild(title);

        // Loading message
        uiContainer.innerHTML += '<h3 id="loading" style="padding-top: 10px">Chargement...</h3>';

        // Invisible div
        var invisible = document.createElement("div");
        invisible.style.display = "none";
        invisible.id = "loading-invisible";
        uiContainer.appendChild(invisible);

        // Reverso div
        var reversoDiv = document.createElement("div");
        reversoDiv.innerHTML +=
            '<h3 style="padding-top: 10px;">Reverso</h3>' +
            '<div class="progressionBar" style="margin: 10px 0;">' +
            '<div style="border-radius: 10px; margin: 0 auto; height: 10px; width:300px;" class="ui-progressbar">' +
            '<div id="reverso-probability" style="width: 0%;" class="ui-progressbar-value"></div>' +
            '</div>' +
            '</div>' +
            '<div id="reverso-info">Problème de communication.</div>';
        reversoDiv.id = "myreversoDiv";
        invisible.appendChild(reversoDiv);

        // Cordial div
        var cordialDiv = document.createElement("div");
        cordialDiv.innerHTML +=
            '<h3 style="padding-top: 10px;">Cordial</h3>' +
            '<div class="progressionBar" style="margin: 10px 0;">' +
            '<div style="border-radius: 10px; margin: 0 auto; height: 10px; width:300px;" class="ui-progressbar">' +
            '<div id="cordial-probability" style="width: 0%;" class="ui-progressbar-value"></div>' +
            '</div>' +
            '</div>' +
            '<div id="cordial-info">Problème de communication.</div>';
        cordialDiv.id = "mycordialDiv";
        invisible.appendChild(cordialDiv);


        // Reponse Voltaire div
        var myvoltaireDiv = document.createElement("div");
        myvoltaireDiv.innerHTML +=
            '<h3 style="padding-top: 10px;">Réponse enregistrée</h3>' +
            '<div class="progressionBar" style="margin: 10px 0;">' +
            '<div style="border-radius: 10px; margin: 0 auto; height: 10px; width:300px;" class="ui-progressbar">' +
            '<div id="voltaire-reponse-probability" style="width: 100%;" class="ui-progressbar-value"></div>' +
            '</div>' +
            '</div>' +
            '<div id="voltaire-reponse-info">Problème de communication.</div>';
        myvoltaireDiv.id = "myvoltaireDiv";
        invisible.appendChild(myvoltaireDiv);

        // Auto
        if (this.auto) {
            document.getElementById('voltaire-solver').innerHTML += '<h2 style="padding-top: 10px; color: red;">Mode automatique activé.</h2>'
        } else {
            document.getElementById('voltaire-solver').innerHTML +=
                '<button id="autobutton" style=" padding: 10px; margin: 10px; background-color: #bfbfbf;" class="top-side-bar-training-exit">Mode automatique</button>';
            document.getElementById('autobutton').addEventListener('click', this.automode.bind(this));
        }
    }

    finishLoading() {
        console.log("Finish loading");
        document.getElementById("loading").remove();
        document.getElementById("loading-invisible").style.display = "";
    }

    requestData(sentence) {
        console.log("Requesting data");
        console.log(sentence);
        this.requestReverso(sentence);
        this.requestCordial(sentence);
    }

    requestReverso(sentence) {
        var self = this;
        console.log("Reverso requesting");
        chrome.runtime.sendMessage({
            command: "reverso",
            phrase: sentence
        }, function (response) {
            console.log("Reverso ok");
            var reverso_substitution = response.reverso_substitution;
            self.reverso_proba = response.reverso_proba;
            var reverso_start = response.reverso_start;
            var reverso_res = "";
            self.place_element_reverso = 0;
            // Create reverso div
            if (self.reverso_proba > 0) {
                reverso_res = sentence.substring(reverso_start, sentence.length);
                reverso_res = reverso_res.split(" ");

                let cpt_char = 0;
                for (let ii = 0; ii < self.mots.length && cpt_char <= reverso_start; ii++) {
                    cpt_char += self.mots[ii].textContent.length;
                    self.place_element_reverso = ii;
                }
                reverso_res = reverso_res[0];
                if (reverso_res != "?" && reverso_res != "." && reverso_res != "," && reverso_res != ".." && reverso_res != "..." && reverso_res != "(" && reverso_res != ")"  && reverso_res != "«" && reverso_res != "»"  && reverso_res != ":") {
                document.getElementById("reverso-info").innerHTML = "Erreur sur '" + reverso_res + "' avec une probabilité de " + self.reverso_proba + "%";
                document.getElementById("reverso-probability").style.width = self.reverso_proba + "%";
                self.mots[self.place_element_reverso].style.color = "red";
                }
                else {
                    self.reverso_proba = 0;
                    document.getElementById("reverso-info").innerHTML = "Pas d'erreur.";
                    document.getElementById("reverso-probability").style.width = "0%";                  
                }

            } else {
                document.getElementById("reverso-info").innerHTML = "Pas d'erreur.";
                document.getElementById("reverso-probability").style.width = "0%";
            }
            self.motauto = null;
            self.reversoLoaded = true;
            console.log("Reverso ok2");
        });
    }

    requestCordial(sentence) {
        var self = this;
        console.log("Cordial requesting");
        chrome.runtime.sendMessage({
            command: "cordial",
            phrase: sentence
        }, function (response) {
            console.log("Cordial ok");
            self.place_element_cordial = 0;
            var returnErrors = response;
            self.cordial_proba = returnErrors[1];
            var cordial_substitution = returnErrors[0];
            if (self.cordial_proba > 0) {
                var cordial_res = sentence.substring(returnErrors[2], sentence.length);
                cordial_res = cordial_res.split(" ");

                let cpt_char = 0;
                for (let ii = 0; ii < self.mots.length && cpt_char <= returnErrors[2]; ii++) {
                    cpt_char += self.mots[ii].textContent.length;
                    self.place_element_cordial = ii;
                }
                cordial_res = cordial_res[0];
                if (cordial_res != "?" && cordial_res != "." && cordial_res != "," && cordial_res != ".." && cordial_res != "..." && cordial_res != "(" && cordial_res != ")" && cordial_res != "«" && cordial_res != "»"  && cordial_res != ":") {
                document.getElementById("cordial-info").innerHTML = "Erreur sur '" + cordial_res + "' avec une probabilité de " + self.cordial_proba + "%";
                document.getElementById("cordial-probability").style.width = self.cordial_proba + "%";
                self.mots[self.place_element_cordial].style.color = "red";
                } 
                else {
                    self.cordial_proba = 0;
                    document.getElementById("cordial-info").innerHTML = "Pas d'erreur.";
                    document.getElementById("cordial-probability").style.width = "0%";
                }
            } else {
                document.getElementById("cordial-info").innerHTML = "Pas d'erreur.";
                document.getElementById("cordial-probability").style.width = "0%";
            }
            self.motauto = null;
            self.cordialLoaded = true;
            console.log("Cordial ok2");
        });
    }

    detectLoaded() {
        console.log("Detect Loaded...");
        if (this.reversoLoaded && this.cordialLoaded) {
            console.log("Loaded.");
            clearInterval(this.loading);
            this.finishLoading();

            if (this.auto){
                setTimeout(this.validateAnswer.bind(this), this.Randomize(this.clickFixedDelay,this.clickRandomDelayRange), null);
            }
        }
    }

    automode() {
        alert('Le mode automatique est maintenant activé. Pour le désactiver, rafraîchissez la page. Vous acceptez les risques liés à ce mode !');
        this.auto = true;
        this.validateAnswer(this.motauto);
    }

    validateAnswer(mot) {
        console.log("Validating answer");
        // Click on element
        var event = document.createEvent("MouseEvent");
        if (mot === null)
        {
            if (this.reverso_proba > 0 && this.cordial_proba > 0) {
                console.log(1);
                if (this.reverso_proba <= this.cordial_proba) {
                    mot = this.mots[this.place_element_cordial];
                    console.log(2);
                } else {
                    mot = this.mots[this.place_element_reverso];
                    console.log(3);
                }
                } else if (this.cordial_proba >= 45) {
                    console.log(4);
                    mot = this.mots[this.place_element_cordial];
                } else if (this.reverso_proba >= 45) {
                    console.log(5);
                    mot = this.mots[this.place_element_reverso];
                } else {
                    console.log(6);
                    mot = document.getElementById("btn_pas_de_faute");
                }
        console.log("Chosen word is " + mot.innerHTML);
        }
        else
        {
            if (mot === -1)
            {
                mot = document.getElementById("btn_pas_de_faute");
            }
            else
            {
                mot = this.mots[mot];
            }
        }
        var rect = mot.getBoundingClientRect();
        var rx = (rect.left + rect.right)/2;
        rx = this.Randomize(rx, mot.offsetWidth/2);
        var ry = (rect.top  + rect.bottom)/2;
        ry = this.Randomize(ry, mot.offsetHeight/2);
        event.initMouseEvent("click", true, false, window, 1, rx, ry, rx, ry, false, false, false, false, 0, null);
        mot.dispatchEvent(event);
        setTimeout(this.next.bind(this), this.Randomize(this.clickFixedDelay,this.clickRandomDelayRange), mot);
        console.log('Clicked');
    }

    next(mot) {
        var next_question = document.getElementById('btn_question_suivante');
        if (next_question != null && next_question.style.display != "none") {

            var place_reponse = -1;
            if (document.getElementsByClassName("answerWord").length != 0)
            {
                var list_mots_reponses = document.getElementsByClassName("answerWord")[0].childNodes;
                var vrai_mot_reponse = document.getElementsByClassName("answerWord")[0].childNodes[0];
                if (list_mots_reponses.length === 3)
                {
                    vrai_mot_reponse = document.getElementsByClassName("answerWord")[0].childNodes[1];
                }
                else if (list_mots_reponses.length === 5)
                {
                    vrai_mot_reponse = document.getElementsByClassName("answerWord")[0].childNodes[3];
                }
                else if (list_mots_reponses.length > 5)
                {
                    if (document.getElementsByClassName("answerWord")[0].childNodes[0].innerHTML != " ")
                    {
                        vrai_mot_reponse = document.getElementsByClassName("answerWord")[0].childNodes[0];
                    }
                    else
                    {
                        vrai_mot_reponse = document.getElementsByClassName("answerWord")[0].childNodes[1];
                    }
                }
                place_reponse = 0;

                while(this.mots[place_reponse] != vrai_mot_reponse)
                {
                    place_reponse++;
                }
            }
            var obj= {};
            obj[this.sentence] = place_reponse;
            if (mot === null)
            {
                self.storage.set(obj);
                console.log("New entry.");       
            }
            next_question.click();
        }
    }
}

var voltaire = new VoltaireSolver();