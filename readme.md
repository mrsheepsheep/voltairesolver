![](https://assets.gitlab-static.net/uploads/-/system/project/avatar/11108500/thumb_u-r-still-an-fagit-deal-with-it-voltaire-age-1180498.png?width=64)

# Téléchargement [ici](http://bit.ly/2BZYR5x)

# Installation
L'extension fonctionne sur Firefox et Chrome au minimum. Elle est non signée, vous devez donc passer en mode débogage pour l'installer.

## Chrome
* Rendez-vous sur chrome://extensions
* Activez le mode développeur en haut à droite
* Posez le fichier .zip dans la fenêtre des extensions
* Si vous avez un doute sur l'IP affichée dans l'avertissement, il s'agit de l'API du site Cordial.

## Firefox
* Rendez-vous sur [about:debugging#addons](about:debugging#addons) en mode débogage
* Cochez la case de débug
* Cliquez sur "Charger un module temporaire"
* Choisissez le fichier .zip de l'extension

# Utilisation
Connectez-vous sur le site du projet voltaire et lancez un parcours ou un test.
L'extension lira la phrase proposée et demandera à Reverso et Cordial les corrections possibles.
Le résultat sera affiché dans la page.

# Avis de non-responsabilité
Cette extension est uniquement un Proof-of-concept. Vous et vous seuls êtes responsables de l'utilisation de cette extension, et des résultats aux tests du projet Voltaire que vous aurez. L'extension peut ne pas fonctionner comme prévu, jusqu'à planter le site du projet voltaire. Vous avez été prévenu.

# Auteurs
Code: Léo

Adaptation en extension: Alexandre